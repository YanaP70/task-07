package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;


public class DBManager {
    private static final String FILE_PROPERTIES = "app.properties";
    private static final String CONNECTION_URL = "connection.url";

    private static DBManager instance;
    private String connectionUrl;
    private Connection connection;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        createConnections();
    }

    private void createConnections() {
        DriverManager.setLogWriter(new PrintWriter(System.out));

        Properties props = new Properties();

        try (InputStream in = new FileInputStream(String.valueOf(Paths.get(FILE_PROPERTIES)))) {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        connectionUrl = props.getProperty(CONNECTION_URL);

        try {
            connection = DriverManager.getConnection(connectionUrl);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        String sql = "SELECT id, login FROM users";

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                list.add(new User(resultSet.getInt("id"), resultSet.getString("login")));
            }
        } catch (SQLException ex) {
            throw new DBException(sql, ex);
        }
        return list;
    }

    public boolean insertUser(User user) throws DBException {
        String sql = String.format("INSERT INTO users VALUES (DEFAULT, '%s')", user.getLogin());
        execUniqueStatement(sql);
        user.setId(getUser(user.getLogin()).getId());
        return true;
    }

    private boolean execUniqueStatement(String sql) throws DBException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLDataException ignored) {

        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        String sql = "DELETE FROM users WHERE id IN ";

        StringJoiner sj = new StringJoiner(", ");
        try (Statement statement = connection.createStatement()) {
            for (User user : users) {
                sj.add(String.valueOf(user.getId()));
            }
            sql = String.format("%s (%s)", sql, sj.toString());
            statement.executeLargeUpdate(sql);
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        String sql = String.format("SELECT id, login FROM users WHERE login = '%s'", login);

        User user = null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                user = new User(resultSet.getInt("id"), resultSet.getString("login"));
            }
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        String sql = String.format("SELECT id, name FROM teams WHERE name = '%s'", name);

        Team team = null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                team = new Team(resultSet.getInt("id"), resultSet.getString("name"));
            }
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        String sql = "SELECT id, name FROM teams";

        try {
            return getListOfTeams(sql);
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        String sql = String.format("INSERT INTO teams VALUES (DEFAULT, '%s')", team.getName());

        execUniqueStatement(sql);
        team.setId(getTeam(team.getName()).getId());
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        try (Connection connectionTr = DriverManager.getConnection(connectionUrl)) {
            connectionTr.setAutoCommit(false);

            try (Statement statement = connectionTr.createStatement()) {

                for (Team team : teams) {
                    String sql = String.format("INSERT INTO users_teams VALUES(%s, %s)", user.getId(), team.getId());
                    statement.addBatch(sql);
                }
                statement.executeBatch();

                connectionTr.commit();
            } catch (SQLException exception) {
                connectionTr.rollback();
                throw exception;
            }
        } catch (SQLException exception) {
            throw new DBException("INSERT INTO users_teams", exception);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String sql = String.format("SELECT id, name FROM teams t WHERE t.id in (SELECT ut.team_id FROM users_teams ut WHERE ut.user_id = %s)", user.getId());

        try {
            return getListOfTeams(sql);
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
    }

    private List<Team> getListOfTeams(String sql) throws SQLException {
        List<Team> list = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                list.add(new Team(resultSet.getInt("id"), resultSet.getString("name")));
            }
        }
        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String sql = String.format("DELETE FROM teams WHERE id =  %s", team.getId());

        return execStatement(sql);
    }

    public boolean updateTeam(Team team) throws DBException {
        String sql = String.format("UPDATE teams SET name = '%s' WHERE id =  %s", team.getName(), team.getId());

        return execStatement(sql);
    }

    private boolean execStatement(String sql) throws DBException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException exception) {
            throw new DBException(sql, exception);
        }
        return true;
    }
}
